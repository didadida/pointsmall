var path = require('path');
var HtmlwebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var autoprefixer = require('autoprefixer')({browsers: ['last 10 Chrome versions', 'last 5 Firefox versions', 'Safari >= 6', 'ie > 8']});
var webpack = require("webpack");
var CopyPlugin=require('copy-webpack-plugin');
var prod = process.env.NODE_ENV === 'production';
var plugins = [];
var disposiHTML;
var entry;
if (prod) {
    plugins.push(new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify('production')
        }
    }));
    plugins.push(new webpack.optimize.CommonsChunkPlugin({name: 'commons'}));
    plugins.push(new webpack.optimize.CommonsChunkPlugin({name: 'manifest', chunks: ['commons']}))
    plugins.push(new CopyPlugin([{
        from:"./src/css/reset.css",
        to:"./css/reset.css"
    }]))
    disposiHTML = "html-withimg-loader";
    entry = {app: "./src/app.js", commons: ["vue"]}
} else {
    plugins.push(new webpack.HotModuleReplacementPlugin())
    disposiHTML = "raw-loader";
    entry = {app: "./src/app.js"}
}

module.exports = {
    entry,
    output: {
        path: path.resolve(__dirname, "dist"),
        filename: prod ? "js/[name].[chunkhash:5].js" : "js/[name].js",
        publicPath:prod?"/":""
    },
    module: {
        rules: [
            {
                test: /\.html$/,
                use: disposiHTML
            },
            {
                test: /\.vue$/,
                use: [
                    {
                        loader: 'vue-loader',
                        options: {
                            loaders: {
                                css: ExtractTextPlugin.extract({
                                    use: ['css-loader'],
                                    fallback: 'vue-style-loader'
                                }),
                                less: ExtractTextPlugin.extract({
                                    use: ['css-loader', 'less-loader'],
                                    fallback: 'vue-style-loader'
                                })
                            },
                            postcss: [autoprefixer]
                        }
                    }
                ]
            },
            {
                test: /\.(eot|svg|ttf|woff)$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            name: 'iconfont/[name].[ext]',
                            limit:10000
                        }
                    }
                ]
            },
            {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
                use: [
                    {
                        loader: 'url-loader',
                        options: {
                            limit: 2000,
                            name: 'images/[name].[ext]'
                        }
                    }
                ]
            },
            {
                test: /\.(less|css)$/,
                use: ExtractTextPlugin.extract([
                    'css-loader',
                    {
                        loader: 'postcss-loader',
                        options: {
                            plugins: [autoprefixer]
                        }
                    },
                    'less-loader'
                ])
            },
            {
                test: /\.js$/,
                use: "babel-loader",
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        extensions:[".vue",".js",".json","less"],
        alias: {
            "vue": "vue/dist/vue.js",
            "com":"./components"
        }
    },
    plugins: [
        new HtmlwebpackPlugin({
            template: "./index.html",
            minify: false,
        }),
        new ExtractTextPlugin("css/style.css")
    ].concat(plugins),
    devServer: {
        historyApiFallback: true,
        hot: true,
        inline: true
    },
    devtool: prod ? false : '#cheap-module-eval-source-map'
}