import Vue from "vue";
import VueRouter from "vue-router";
Vue.use(VueRouter);
import axios from "axios";
axios.defaults.timeout =15000;
axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
axios.interceptors.request.use((config) => {
	//请求发送前开启loading
	vm.$root.loading = true;
	vm.$root.type = 0;
	vm.$root.doSomething = function () {
		window.location.reload();
	};
	return config
});
axios.interceptors.response.use(res => {
	//响应成功关闭loading
	vm.$root.loading = false
	return res
}, err => {
	//响应失败开启提示窗
	console.log("err",err);
	vm.$root.tip = true;
	vm.$root.tipTxt = "获取数据失败，点击确定刷新重试";
	return Promise.reject(err);
});
Vue.prototype.$http = axios;
Vue.prototype.$url = function (key, query = "") {
	return `http://www.zhonglingfeng.cn/weixin/${key}.shtml?${query}`
};

import app from "./app";
import integral from "com/integral";
import myCenter from "com/mycenter";
import goodsInfo from "com/goodsInfo";
import queryExpressResult from "com/queryExpressResult";
import myBill from "com/myBill";
import exchange from "com/exchange";
import expenseRecord from "com/expenseRecord";
import newRecord from "com/newRecord";
import submitOrder from "com/submitOrder";
import myAddress from "com/myAddress";
import notice from "com/notice";

import iconfont from "./iconfont/iconfont.css";

const router = new VueRouter({
	mode: "hash",
	routes: [
		{
			path: '/integral',
			component: integral
		},
		{
			path: '/notice',
			component: notice
		},
		{
			path: '/myaddress',
			component: myAddress
		},
		{
			path: '/submitorder',
			component: submitOrder
		},
		{
			path: '/newrecord',
			component: newRecord
		},
		{
			path: '/expenserecord',
			component: expenseRecord
		},
		{
			path: '/exchange',
			component: exchange
		},
		{
			path: '/mybill',
			component: myBill
		},
		{
			path: '/queryexpressresult',
			component: queryExpressResult
		},
		{
			path: '/goodsinfo',
			component: goodsInfo
		},
		{
			path: '/mycenter',
			component: myCenter
		}
	]
})

router.beforeEach((to, from, next) => {
	if (vm) {
		if (to.path === "/") {
			vm.$children[0].nav = true
		} else {
			vm.$children[0].nav = false
		}
	}
	next();
})


const vm = new Vue({
	el: "#app",
	router,
	render: h => h(app),
	data: {
		loading: false,
		tip: false,
		tipTxt: "",
		twoBtn: false
	},
	methods:{
		doSomething(){
			console.log(1)
		}
	},
	beforeCreate(){
		document.documentElement.style.fontSize = document.documentElement.clientWidth / 7.5 + "px"
	},
	mounted(){
		if (this.$route.path !== "/") {
			this.$children[0].nav = false
		}
	}
})
